# QUIC Performance on 10G Links
Bachelor's Thesis for B.Sc. in Computer Science at Technical University of Munich (TUM).

## Abstract
This work aims to shed light on the performance of QUIC and the CPU cost associated with its components in single-connection scenarios dictated by the congestion avoidance phase. 
Fitting to QUICs initial design motivation to improve web performance, many works focus on the use case of short measurement scenarios, favoring a fast connection setup. 
We argue that for QUIC to be usable as the general purpose transport layer protocol it was recently standardized as, we require more data on longer transfers with handshakes playing less of a role, and where a transport service user would likely turn to Transmission Control Protocol (TCP). 
We realized structured, reproducible measurements by extending the QUIC Interop Runner measurement framework to work for bare-metal experiments on real, physically distinct machines. 
We found that in long-duration scenarios, QUIC achieved only half the goodput of TCP+Transport Layer Security (TLS) when excluding Network Interface Card (NIC) offloading, and about one-third of it with offloading support for TCP. 
Both QUIC and TCP transfers reached 100 % CPU utilization on the server for the core they ran on. 
When looking at how QUIC uses its CPU time, we identified packet I/O as the most expensive component regarding CPU utilization, followed by crypto operations. 
Using this approach of a structured framework based on QUIC interoperability allows for easier, future hardware measurements in needed areas like flow control, where variables like ACK frequency are not yet unified by a standard document.